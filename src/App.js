import React, { Component, Fragment } from 'react';

import products from './utils';
import { ProductsItem } from './components/ProductsItem'


export  class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showId: ''
    };
    this.renderProducts = this.renderProducts.bind(this)
  }

  onMouseEntered = (id) => {
    this.setState({
      showId: id
    })
  }


  renderProducts = () => {
    const { showId } = this.state;
    return products.map(product => {
      return (
        <ProductsItem
          key={product.id}
          onMouseEntered={this.onMouseEntered}
          product={product}
          showId={showId}
        />
      )
    })
  }

  render() {
    return (
      <div className="wrapper">
        <article className="content">
          { this.renderProducts() }
        </article>
      </div>
    )
  }
}

export default App
