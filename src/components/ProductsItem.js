import React from 'react';

import converter from "../public/assets/converters.jpg";
import heart from "../public/assets/heart.png";
import magic from "../public/assets/magic.png";
import arrow from "../public/assets/arrow.png";
import '../style.css';

export const ProductsItem = (props) => {
  const {
    onMouseEntered,
    product,
    showId
  } = props;
  return (
    <div
      onMouseOver={() => {
        onMouseEntered(product.id)
      }}
      onMouseOut={() => {
        onMouseEntered("")
      }}
      key={product.id}
      className={"product"}
    >
      <p>{product.name}</p>
      <img className="img-responsive" src={converter} alt="converters.jpg"/>
      <div className="thee-images">
        <img src={heart} alt="heart"/>
        <img src={arrow} alt="arrow"/>
        <img src={magic} alt="magic"/>
      </div>
      <div
        className={showId === product.id ? "hovered-div" : "hovered-div disabled"}
      >
        <div>
          <p>Pressure Rating</p> <p>{product.pressure_rating}</p>
        </div>
        <div>
          <p>Waltage Rating</p> <p>{product.voltage_rating}</p>
        </div>
        <div>
          <p>Wire Gauges</p> <p>{product.wiregauge}</p>
        </div>
        <div>
          <p>Contacts</p> <p>{product.contacts}</p>
        </div>
      </div>
    </div>
  )
}

