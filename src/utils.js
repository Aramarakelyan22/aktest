const products = [
  {
    "id": 963751069058,
    "name": "55A2-1610",
    "description": "'5500 Series, Attachable, Sz 16, 10 contact, Dry Mate Receptacle'",
    "pressure_rating": "10K",
    "voltage_rating": 600,
    "wiregauge": 20,
    "contacts": "10",
    "jpeg_file_url": "/converters.jpg",
    "gender": "Male"
  },
  {
    "id": 963751069067,
    "name": "55A2-1610",
    "description": "'5500 Series, Attachable, Sz 16, 10 contact, Dry Mate Receptacle'",
    "pressure_rating": "10K",
    "voltage_rating": 600,
    "wiregauge": 20,
    "contacts": "10",
    "jpeg_file_url": "/converters.jpg",
    "gender": "Male"
  },
  {
    "id": 963751069157,
    "name": "55A2-1610",
    "description": "'5500 Series, Attachable, Sz 16, 10 contact, Dry Mate Receptacle'",
    "pressure_rating": "10K",
    "voltage_rating": 600,
    "wiregauge": 20,
    "contacts": "10",
    "jpeg_file_url": "/converters.jpg",
    "gender": "Male"
  },
  {
    "id": 963751060057,
    "name": "55A2-1610",
    "description": "'5500 Series, Attachable, Sz 16, 10 contact, Dry Mate Receptacle'",
    "pressure_rating": "10K",
    "voltage_rating": 600,
    "wiregauge": 20,
    "contacts": "10",
    "jpeg_file_url": "/converters.jpg",
    "gender": "Male"
  },
  {
    "id": 963751079057,
    "name": "55A2-1610",
    "description": "'5500 Series, Attachable, Sz 16, 10 contact, Dry Mate Receptacle'",
    "pressure_rating": "10K",
    "voltage_rating": 600,
    "wiregauge": 20,
    "contacts": "10",
    "jpeg_file_url": "/converters.jpg",
    "gender": "Male"
  }
];


export default products